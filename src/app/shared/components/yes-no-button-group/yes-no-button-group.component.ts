import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {UniqueIdService} from '../../services/unique-id.service';

@Component({
  selector: 'app-yes-no-button-group',
  templateUrl: './yes-no-button-group.component.html',
  styleUrls: ['./yes-no-button-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => YesNoButtonGroupComponent)
    }
  ]
})
export class YesNoButtonGroupComponent implements ControlValueAccessor, OnInit {
  @Input() public disabled = false;
  @Input() public value = 'no';
  @Input() public label = '';
  @Output() public valueChange = new EventEmitter();
  public id?: string;
  public options = YesNoButtonGroupOptions;
  public onChange = (value: string) => {};
  public onTouched = (value: string) => {};

  constructor(private uniqueIdService: UniqueIdService) {}

  ngOnInit(): void {
    this.id = this.uniqueIdService.generateUniqueIdWithPrefix(
      'yes-no-button-group'
    );
  }
  public activate(value: string) {
    this.writeValue(value);
  }

  public writeValue(value: string): void {
    this.value = value;
    this.onChange(this.value);
    this.valueChange.emit(this.value);
  }

  public registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}

enum YesNoButtonGroupOptions {
  YES = 'yes',
  NO = 'no'
}
