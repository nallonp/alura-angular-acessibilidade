import {Injectable} from '@angular/core';
import {v1} from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class UniqueIdService {
  public generateUniqueIdWithPrefix(prefix: string) {
    const uniqueID = UniqueIdService.generateUniqueId();
    return `${prefix}-${uniqueID}`;
  }

  private static generateUniqueId(): string {
    return v1();
  }
}
